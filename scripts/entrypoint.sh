#!/bin/sh

if [ $# -gt 0 ]; then
	# custom
	exec $@
else
	# default (the same as "tox -r runserver" but without venv
	cd "${HORIZON_HOME}" \
	&& ./manage.py collectstatic --noinput --clear --verbosity 0 \
	&& ./manage.py compress --force --verbosity 0 \
	&& exec ./manage.py runserver --insecure --verbosity 0 "0.0.0.0:${HORIZON_PORT:-8000}"
fi
