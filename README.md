# OpenStack Horizon Docker Image

A docker image for OpenStack Horizon.

## References

*	[docker-openstack-horizon](https://github.com/alvaroaleman/docker-openstack-horizon)
*	[Quickstart](https://docs.openstack.org/horizon/latest/contributor/quickstart.html)

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-openstack-horizon:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-openstack-horizon" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).
